#include <stdio.h>
#include <pthread.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>
#include <sys/neutrino.h>

int main (void)
{
	char smsg[20];
	char rmsg[200];
	int coid;
	long serv_pid;
	printf("Client, Write PID of server \n");
	scanf("%ld", &serv_pid);
	printf("Server id is %ld \n", serv_pid);	
	coid = ConnectAttach(0, serv_pid, 1, 0, 0);
	printf("Connect result is %d , write message ", coid);
	scanf("%s", &smsg);
	
	printf("\nYour message is %s \n", smsg);
	if(MsgSend(coid, smsg, strlen(smsg) + 1, rmsg, sizeof(rmsg)) == -1)
	{
		printf("Error MsgSend \n");
	}
	printf("\nServer return: %s\n",rmsg);
	
	return (1);
}
