#include <stdio.h>
#include <pthread.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>
#include <sys/neutrino.h>


void server(void)
{
	int rcvid;
	int chid;
	char message[500];
	char answer[550];
	printf("Server starts working");
	
	
	chid = ChannelCreate(0);
	printf("Channel id : %d \n", chid);
	printf("Pid : %d \n", getpid());
	
	while(1)
	{
		rcvid = MsgReceive(chid, message, sizeof(message), NULL);
		printf("You got message, rcvid %d, \n", rcvid);
		printf("Message is : %s \n", message);
		
		strcpy(answer, "You send me:");
		strcat(answer, message);
		MsgReply(rcvid, EOK, answer, sizeof(answer));
		//printf("%s \n", message);
	}
}

int main (void)
{
	printf("Server \n");
	server();
	sleep(5);
	return (1);
}
		
